# Scanner Shield Firmware

Arduino Firmware for the Holographic Video Monitor Scanner Shield. The code contains some hardware-specific instructions and be used with an Arduino Mega 2560 (or other ATMega 2560 based board).

The physical Scanner Shield fits onto the headers of the Arduino, and running this firmware can operate the scanning system of the Holographic Video Monitor.
Once started, connect to the board over a Serial terminal to manually synchronize the scanning system. While a prototype automatic locking control is implemented (in branch AutoLock), it's still in alpha stages and isn't recommended for use. Over the serial terminal, the following controls are available:
 * 'h' - Hot-Plug enable (toggles the pin indicating to the computer that a monitor is attached) - High-Asserted
 * 'm' - Motor Enable (toggles the polygon motor enable pin) - Low-Asserted
 * Four keys for commanding motor speed (which is an unsigned integer 0-4095):
  * '8' and '2' (up and down, respectively, on a 10-key pad): coarse control, ±20 per keypress (up to increase, down to decrease)
  * '4' and '6' (left and right, respectively, on a 10-key pad): fine control, ±1 per keypress (right to increase, left to decrease)
 * Some branches (not master, currently) may also have the following options (all High-Asserted):
  * 'o' - Output enable (toggles measurement output to the Serial console)
  * 'a' - Auto Control enable (toggles the AutoLock algorithm as a whole)
  * 'd' - Counter Data Collection enable (toggles the raw binary data output from DrewDataCollection)
  * 'c' - Period Control enable (toggles the period-only based AutoLock algorithm)
  * 'e' - Phase Filter enable (toggles certain filters for phase measurements within the AutoLock code)
  * 'f' - Verbose Data Filtering (toggles filters on raw data output)
  * 'i' - DSYNC Phase Invert (Inverts the software Divide-by-2 of the DSYNC signal, used when the system locks 180° out of phase)
  * 'l' - Loop Cycle Reporting (toggles reports for each complete cycle of the main loop)
  * 'p' - Phase Filter Report enable (toggles reports from the filters enabled by 'e')

# License

All files in this repository, including source code and auxiliary references, are Copyright 2016-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
