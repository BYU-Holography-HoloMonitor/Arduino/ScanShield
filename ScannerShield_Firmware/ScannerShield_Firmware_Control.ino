void PLL_controller_update()
{
  // Commands:
  // phase_command = 0
  // phase_delta_command = 0
  
  // period = 1 / frequency, which can linearly approximated in a local regime
  // DSYNC_value = 16,000,000 / f_DSYNC
  // PTACH_value = 16,000,000 / f_Poly
  // frequency = time-derivative of phase   aka   phase = integral of frequency
  
  // let's just make things signed as opposed to unsigned to simplify things
  
  // trigger on DSYNC since it occurs at a regular rate
  
  // wait for the motor to be spinning fast enough that measurements are valid
  //   and until we're in the valid control range
  if( !spinup_wait && PTACH_period_filtered < 16667 )
  {
    // 16MHz / 960Hz = 16667 clock cycles
    // wait until the motor has been given enough time to spin up for us to reliably measure PTACH_period
    // and rotational speed is within the valid control range ( 0.960 kHz < f_PTACH < 1.71 kHz, the polygon motor operational range )
    
    
    // Controller: Feed-Forward term
    
    // based upon the Sat 12 Dec 2015 study, 
    // the polygon mirror motor has these characteristics:
    
    // Tachometer Freq (kHz)
    //        Motor Voltage (V)
    //               Current Consumption (mA)
    // 1.71   16.3   240
    // 1.68   15.9   210
    // 1.6    15.1   200
    // 1.52   14.2   190
    // 1.44   13.6   180
    // 1.36   12.8   170
    // 1.28   12.1   160
    // 1.2    11.2   160
    // 1.12   10.4   150
    // 1.04    9.7   150
    // 0.96    9.1   150
    
    // the double op-amp circuitry going from the DAC is meant to scale command values between
    // 0 - 4095 (0% - 100%) to this voltage range (9.1V - 16.3V)
    // there will be some offsets based upon normal resistor value deviation
    
    // throttle %     PTACH count
    //         DAC Command     f_poly      voltage
    // 100 %   4095    9,356   1.710 kHz   16.3 V
    //  75 %   3072   10,509   1.523 kHz   
    //  50 %   2048   11,985   1.335 kHz   
    //  25 %   1024   13,943   1.148 kHz   
    //   0 %      0   16,667   0.960 kHz    9.6 V
    
    // the relationship between input voltage and output frequency is pretty linear between 9.6 V and 16.3 V
    
    // implement feed-forward function
    
    // since the relationship between PTACH and DAC Command (motor_value)
    // is inversely proportional (nonlinear and gently curving away from the origin)
    // make a simple linear approximation that minimizes error along the entire spectrum
    //  => linearize between 25% and 75%
    //
    // slope:
    //
    //   25%   75%
    //
    //  1024 - 3072
    // ------------- = -0.59638905067...
    // 13943 - 10509
    //
    // this number (ignoring the negative) is represented in binary as
    // 0.1001100010101100...
    //
    // truncated to 4 ones:
    // 0.100110001
    //  = 0.595703125
    //
    // y-intercept with 25% and untruncated slope:
    // 1024 = -0.596389 * 13943 + ?
    // 1024 + 0.596389 * 13943 = 9339.45...
    //
    // the integral term will make up for any errors
    
    // feed_forward_motor_value = -0.59638 * DSYNC_period_filtered + 9339;
    
    // fixed-point decimal-binary converter:
    // http://www.exploringbinary.com/binary-converter/
    
    // 0.59638     (0.1001100010101100...)
    // can be approximated as
    // 0.595703125 (0.100110001)
    //                1  45   9
  
    int feed_forward_motor_value = 0;
    
    feed_forward_motor_value += DSYNC_period_filtered >> 1;
    feed_forward_motor_value += DSYNC_period_filtered >> 4;
    feed_forward_motor_value += DSYNC_period_filtered >> 5;
    feed_forward_motor_value += DSYNC_period_filtered >> 9;
    
    feed_forward_motor_value = -feed_forward_motor_value;
    
    feed_forward_motor_value += 9339;
    
    motor_value = feed_forward_motor_value;
    

    
    
    period_error = PTACH_period_filtered - DSYNC_period_filtered;
    
    // since period and command have a negative relation (inversely proportional),
    // kp and ki need to be negative, so just use PTACH period - DSYNC period instead
    
    
    
    // Controller: Period Proportional Term
    {
      if( period_error < -2 || 2 < period_error )
      {
        // deadband small error to reduce the amount of datapoints in the telemetry
        // the integral and hardware will take care small errors
        kp_period_term = period_error << 3; // experimentally determined
      }
      // else keep previously-set kp_period_term value
      
      motor_value += kp_period_term;
    }
    
    
    
    // wait until the phase (delta) stops aliasing
    
    if( -DSYNC_period_filtered_eighth < period_error && period_error < DSYNC_period_filtered_eighth )
    {
      
      // note that PHASE_delta (new phase - old phase) = PTACH_period - DSYNC_period
      // I think I should use PHASE_delta instead of PTACH_period - DSYNC_period
      // because PHASE_delta is impervious to TCNT4/TCNT5 clock init offset mistakes
      // but PHASE_delta *aliases* if:
      //   PTACH_period - DSYNC_period < -DSYNC_period / 4   ||   DSYNC_period / 4 < PTACH_period - DSYNC_period
      
      //if( -DSYNC_period_filtered_quarter < period_error && period_error < DSYNC_period_filtered_quarter )
      //{
        // if the period error is small enough so that the phase is no longer aliasing,
        //   switch to a slightly more accurate technique
      
        //period_error = PHASE_delta;
        // over 31,475 samples, PHASE_delta has the following distribution in the lock state:
        // -2  0.1%
        // -1 20.0%
        //  0 59.9%
        //  1 19.9%
        //  0  0.2%
      //}
      
      
      
      
      // Controller: Period Integral Term
      {        
        // according to Fri 3 Nov 2017 measurements, under normal lock conditions,
        //   PHASE_value will have the following distribution: (measured over 15,398 samples, init TCNT=2)
        //
        //
        //                     .  |
        //               .  |  |  |  |
        //            |  |  |  |  |  |  |
        //         |  |  |  |  |  |  |  |  |  
        //   .  |  |  |  |  |  |  |  |  |  |  |  .
        //  -7 -6 -5 -4 -3 -2 -1  0  1  2  3  4  5
        //
        // so include a deadband so that the integral term isn't being adjusted when it's locked
        // actually, a deadband isn't needed; it appears that the errors average-out
        
        // prevent wind-up by only allowing accumulations to occur after the system has stabilized enough

        if( ki_period_enable )
          ki_period_term_long_sum += period_error;
        else
          if( PHASE_acceleration == 0 )
            if( PHASE_acceleration_zero_count < 20 ) // 20 experimentally determined; value has nonlinear effect
              PHASE_acceleration_zero_count++;
            else
            {
              ki_period_enable = true;
              Serial.println( "\nPeI ENA" );
            }
          else
            if( PHASE_acceleration_zero_count > 0 )
              PHASE_acceleration_zero_count--;
        
        if( ki_period_term_long_sum >  1000000000L )
            ki_period_term_long_sum =  1000000000L;
        if( ki_period_term_long_sum < -1000000000L )
            ki_period_term_long_sum = -1000000000L;
        
        ki_period_term = ( ki_period_term_long_sum >> 7 ); // experimentally determined
        
        motor_value += ki_period_term;
      }
      
      
      
      // Controller: Phase Integral Term
      // this is almost never needed except when the Scanner Shield power transistor begins to overheat
      //   when this happens, the phase will oscillate, and the period integrator doesn't fix it
      if( ki_period_enable )
      {
        // reset the integrator if we go from -180 to +180 or vice-versa
        if( ( ( PHASE_value < 0 ) != ( PHASE_value_last < 0 ) ) &&
            ( PHASE_value < -DSYNC_period_filtered_quarter || DSYNC_period_filtered_quarter < PHASE_value ) )
          ki_phase_term_long_sum = 0L;
        
        if( PHASE_value < -10 || 10 < PHASE_value )
          ki_phase_term_long_sum += PHASE_value;
      
        if( ki_phase_term_long_sum >  1000000L ) // enforce a smaller maximum/minimum to limit windup
            ki_phase_term_long_sum =  1000000L;  //   otherwise it'll take more time to undo windup and oscillate a bunch
        if( ki_phase_term_long_sum < -1000000L )
            ki_phase_term_long_sum = -1000000L;
        
        ki_phase_term = ( ki_phase_term_long_sum >> 14 ); // experimentally determined
        
        motor_value += ki_phase_term;
      }
    }
  }
  else
  {
    motor_value = 4095; // command maximum torque during spinup
    
    ki_period_enable = false;
    PHASE_acceleration_zero_count = 0;
    ki_period_term_long_sum = 0L;
    
    ki_phase_term_long_sum = 0L;
  }
  
  // the motor value will be automatically clipped and sent to the DAC
  // see the code reference by "LABEL: MOTOR_VALUE UPDATE"
}