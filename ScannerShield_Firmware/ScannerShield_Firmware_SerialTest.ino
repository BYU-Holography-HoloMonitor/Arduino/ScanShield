/////////////////////////////////////
//
// SERIAL TESTING
//
/////////////////////////////////////

// this uses Serial.unsafe_direct_write_array(...), which is a modification of the Arduino Serial hardware library
// this is to minimize the number of clock cycles it takes to load data into the hardware serial transmit array
// modified by Andrew Henrie 29 July 2017
//   directory: C:\Users\[user]\AppData\Local\Arduino15\packages\arduino\hardware\avr\1.6.19\cores\arduino\
//   files modified:
//     HardwareSerial.h
//     HardwareSerial.cpp


void printCharacterMapping()
{
  // Serial character mapping
  Serial.println("printing test characters...");
  int i;
  for ( i = 0; i < 256; i++ )
  {
    Serial.print( "0x" );
    Serial.print( i, HEX );
    Serial.print( " '" );
    Serial.print( (char) i );
    Serial.println( "'" );
  }
  
  /*
  with default TeraTerm settings:
  
  0x07 - bell
  0x08 - delete following character
  0x09 - tabs to the next multiple of 8 position
  0x0A - move the caret down directly; not to the beginning of the line
  0x0B - same as 0x0A
  0x0C - same as 0x0A
  0x0D - returns to beginning of current line, not the next line
  0x0F - weird; prints 0|F ''
  0x1B - delete next character
  0x1C - after done printing, go to beginning of line and delete a character (?)
  
  */
}



void test_serial()
{
  unsigned char i = 8;
  unsigned char j;

  Serial.println( "\nTab spacing\n" );
  
  while( i-- )
  {
    Serial.print( '|' );
    
    j = 9;
    while( j-- )
      Serial.print( ' ' );
  }
  
  Serial.println( "\r\nTabbing to next position\n" );
  
  for( i=0; i<20; i++ )
  {
    for( j=0; j<i; j++ )
    {
      Serial.print( '.' );
    }
    Serial.print( (char) 0x09 );
    Serial.print( '|' );
    
    Serial.println();
  }
  
  
  Serial.println( "\nDo tabs overwrite previous characters?\n" );
  
  Serial.print( "1234567890123456789" );
  Serial.print( "\r" ); // (char) 0x0D
  Serial.print( "\t-\t-" );
  Serial.println();
  // prints "12345678-0123456-89", so tabs do NOT overwrite previous characters
  
  
  Serial.println( "\r\n\r\n");
  
  Serial.println( "Measuring clock cycle cost - default Arduino functions" );
  
  unsigned int temp_clock_start, temp_clock_stop;
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( "\"\" Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 6 clock cycles
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.print( "a" );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 324 clock cycles, 500,000 baud
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.print( "ab" );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 569 clock cycles, 500,000 baud
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.print( "abc" );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 681 clock cycles, 500,000 baud
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.print( "abcd" );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 926 clock cycles, 500,000 baud
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.print( "abcdefghijklmnopqrstuvwxyz" );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 5219 clock cycles, 500,000 baud
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.write( "1", 1 );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 318 clock cycles, 500,000 baud
  
  
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  Serial.write( "12345678", 8 );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.println( " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); // 1782 clock cycles, 500,000 baud
  
  
  
  Serial.println( "\nCurrent #DEFINE's and registers:\n" );
  
  /*
  This serial is stupidly show. 324 clock cycles to transmit a single character with Serial.print()?!?!
  Transmission is supposed to be interrupt-driven; check that it is.
   - it is.
  
  Hardware serial transmission interrupts is discussed in Sec. 22.6.3, pg. 208 of ATmega2560 datasheet
  
  26   $0032   USART0 RX     USART0 Rx Complete
  27   $0034   USART0 UDRE   USART0 Data Register Empty
  28   $0036   USART0 TX     USART0 Tx Complete
  */
  
  // is hardware serial available? (HardwareSerial0.cpp)
  Serial.print( "HAVE_HWSERIAL0 " );
  #if defined(HAVE_HWSERIAL0)
    Serial.println( "yes" ); // <==
  #else
    Serial.println( "no" );
  #endif
  
  // is the needed ISR vector available?
  Serial.print( "UART0_UDRE_vect " );
  #if defined(UART0_UDRE_vect)
    Serial.println( "yes" );
  #else
    Serial.println( "no" ); // <==
  #endif
  
  Serial.print( "UART_UDRE_vect " );
  #if defined(UART_UDRE_vect)
    Serial.println( "yes" );
  #else
    Serial.println( "no" ); // <==
  #endif
  
  Serial.print( "USART0_UDRE_vect " );
  #if defined(USART0_UDRE_vect)
    Serial.println( "yes" ); // <==
  #else
    Serial.println( "no" );
  #endif
  
  Serial.print( "USART_UDRE_vect " );
  #if defined(USART_UDRE_vect)
    Serial.println( "yes" );
  #else
    Serial.println( "no" ); // <==
  #endif
  
  // are the USART0 Data Register Empty (UDRIE0) and Transmit Complete (TXCIE0) interrupt vectors enabled?
  // Register UCSR0B: [  RXCIE0 TXCIE0 UDRIE0 RXEN0  TXEN0 UCSZ02 RXB80 TXB80  ]
  // for UCSRnB description, see Sec. 23.6.3, pg. 234
  
  // A Data Register Empty interrupt will be generated only if 
  //   the UDRIE bit is written to one,
  //   the Global Interrupt Flag in SREG is written to one and 
  //   the UDREn bit in UCSRnA is set.
  //  - description of UDRIE, Sec. 23.6.3, pg. 234
  
  Serial.print( "UCSR0B: 0x" );
  Serial.println( UCSR0B, HEX ); // 0xB8 = 1011 1000 - UDRIE0 is enabled
  Serial.print( "SREG: 0x" );
  Serial.println( SREG, HEX ); // 0x82 - global interrupts are enabled
  Serial.print( "UCSR0A: 0x" );
  Serial.println( UCSR0A, HEX ); // 0x02 - the transmit buffer UDR0 was not empty at this time
  
  // ISR(USART0_UDRE_vect) is in HardwareSerial0.cpp and only calls
  // Serial._rx_complete_irq(); in HardwareSerial.cpp
  
  // does the Arduino code dynamically enable/disable UDRIE0 and TXCIE0?
  // is this something we have to do ourselves?
  //  - it enables / disables UDRIE0 with 
  //    sbi(*_ucsrb, UDRIE0) / cbi(*_ucsrb, UDRIE0)
  //    respectively at the appropriate places in HardwareSerial.cpp
  
  
  
  Serial.println( "\r\n\r\n");
  
  Serial.println( "verifying printDecConstWidth(), positive numbers, unsigned" );
  
  Serial.println( printDecConstWidth(     0, 0 ) ); //     0
  Serial.println( printDecConstWidth(     1, 0 ) ); //     1
  Serial.println( printDecConstWidth(     9, 0 ) ); //     9
  Serial.println( printDecConstWidth(    10, 0 ) ); //    10
  Serial.println( printDecConstWidth(    87, 0 ) ); //    87
  Serial.println( printDecConstWidth(   100, 0 ) ); //   100
  Serial.println( printDecConstWidth(   654, 0 ) ); //   654
  Serial.println( printDecConstWidth(  1000, 0 ) ); //  1000
  Serial.println( printDecConstWidth(  3210, 0 ) ); //  3210
  Serial.println( printDecConstWidth( 10000, 0 ) ); // 10000
  Serial.println( printDecConstWidth( 43210, 0 ) ); // 43210
  Serial.println( printDecConstWidth( 65535, 0 ) ); // 65536
  
  Serial.println( "verifying printDecConstWidth(), positive numbers, signed" );
  
  Serial.println( printDecConstWidth(     0, 1 ) ); //      0
  Serial.println( printDecConstWidth(     1, 1 ) ); //      1
  Serial.println( printDecConstWidth(     9, 1 ) ); //      9
  Serial.println( printDecConstWidth(    10, 1 ) ); //     10
  Serial.println( printDecConstWidth(    87, 1 ) ); //     87
  Serial.println( printDecConstWidth(   100, 1 ) ); //    100
  Serial.println( printDecConstWidth(   654, 1 ) ); //    654
  Serial.println( printDecConstWidth(  1000, 1 ) ); //   1000
  Serial.println( printDecConstWidth(  3210, 1 ) ); //   3210
  Serial.println( printDecConstWidth( 10000, 1 ) ); //  10000
  Serial.println( printDecConstWidth( 43210, 1 ) ); // -22326
  Serial.println( printDecConstWidth( 65535, 1 ) ); //     -1
  
  Serial.println( "verifying printDecConstWidth(), negative numbers, signed" );
  
  Serial.println( printDecConstWidth(      0, 1 ) ); //      0
  Serial.println( printDecConstWidth(     -1, 1 ) ); //     -1
  Serial.println( printDecConstWidth(     -9, 1 ) ); //     -9
  Serial.println( printDecConstWidth(    -10, 1 ) ); //    -10
  Serial.println( printDecConstWidth(    -87, 1 ) ); //    -87
  Serial.println( printDecConstWidth(   -100, 1 ) ); //   -100
  Serial.println( printDecConstWidth(   -654, 1 ) ); //   -654
  Serial.println( printDecConstWidth(  -1000, 1 ) ); //  -1000
  Serial.println( printDecConstWidth(  -4567, 1 ) ); //  -4567
  Serial.println( printDecConstWidth( -10000, 1 ) ); // -10000
  Serial.println( printDecConstWidth( -21098, 1 ) ); // -21098
  Serial.println( printDecConstWidth( -32768, 1 ) ); // -32768
  Serial.println( printDecConstWidth(  32767, 1 ) ); //  32767
  Serial.println( printDecConstWidth(  32768, 1 ) ); // -32768
  
  
  
  Serial.println( "\r\n\r\n");
  
  
  
  // test of unsafe_direct_write_array
  Serial.print( "test of unsafe_direct_write_array; non-interruptable; SERIAL_TX_BUFFER_SIZE = " );
  Serial.println( String( SERIAL_TX_BUFFER_SIZE ) );
  Serial.flush();
  
  Serial.disable_interrupts_in_UDRE_ISR();
  
  unsigned char test_i = 1;
  char temp_c_str[ 30 ] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  unsigned char n_characters_printed = 0;
  
  while( test_i <= 30 )
  {
    n_characters_printed = 0;
    
    noInterrupts();
    temp_clock_start = TCNT3;
    interrupts();
    
    n_characters_printed += Serial.unsafe_direct_write_array( temp_c_str, test_i );
    
    noInterrupts();
    temp_clock_stop = TCNT3;
    interrupts();
    
    Serial.flush();
    Serial.println( "\r\nCharacters: " + String( n_characters_printed ) + " Cycles: " + String( temp_clock_stop - temp_clock_start ) );
    Serial.flush();
    
    test_i++;
  }
  
  Serial.println( "multipart" );
  Serial.flush();

  n_characters_printed = 0;
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.flush();
  Serial.println( "\r\nCharacters: " + String( n_characters_printed ) + " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); //  clock cycles,  delta 500,000 baud
  
  
  
  // test of unsafe_direct_write_array
  Serial.print( "test of unsafe_direct_write_array; INTERRUPTABLE; SERIAL_TX_BUFFER_SIZE = " );
  Serial.println( String( SERIAL_TX_BUFFER_SIZE ) );
  Serial.flush();
  
  Serial.enable_interrupts_in_UDRE_ISR();
  
  test_i = 1;
  //char temp_c_str[ 30 ] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  
  while( test_i <= 30 )
  {
    n_characters_printed = 0;
  
    noInterrupts();
    temp_clock_start = TCNT3;
    interrupts();
    
    n_characters_printed += Serial.unsafe_direct_write_array( temp_c_str, test_i );
    // include the += operation as well in the measurement
    
    noInterrupts();
    temp_clock_stop = TCNT3;
    interrupts();
    
    Serial.flush();
    Serial.println( "\r\nCharacters: " + String( n_characters_printed ) + " Cycles: " + String( temp_clock_stop - temp_clock_start ) );
    Serial.flush();
    
    test_i++;
  }
  
  Serial.println( "multipart" );
  Serial.flush();

  n_characters_printed = 0;
  
  noInterrupts();
  temp_clock_start = TCNT3;
  interrupts();
  
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  n_characters_printed += Serial.unsafe_direct_write_array( "ABCDEFGH", 8 );
  
  noInterrupts();
  temp_clock_stop = TCNT3;
  interrupts();
  
  Serial.flush();
  Serial.println( "\r\nCharacters: " + String( n_characters_printed ) + " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); //  clock cycles,  delta 500,000 baud
  
  Serial.disable_interrupts_in_UDRE_ISR();
  
  Serial.flush();
  
  
  
  // test of code similar to application
  Serial.println( "\nApplication-similar code" );
  
  unsigned int temp_uint16 = 0x1007;
  char temp_char = 'a';
  test_i = 10;
  
  while( test_i-- )
  {
    n_characters_printed = 0;
    
    noInterrupts();
    temp_clock_start = TCNT3;
    interrupts();
    
    n_characters_printed += Serial.unsafe_direct_write_array( printCounterData( temp_char, temp_uint16 ), 5 );
    
    noInterrupts();
    temp_clock_stop = TCNT3;
    interrupts();
    
    Serial.flush();
    Serial.println( "\r\nCharacters: " + String( n_characters_printed ) + " Cycles: " + String( temp_clock_stop - temp_clock_start ) ); //  clock cycles,  delta 500,000 baud
    Serial.flush();
    
    temp_uint16 += temp_uint16 << 1;
    temp_char += 1;
  }
}