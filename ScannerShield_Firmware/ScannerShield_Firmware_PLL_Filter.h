/////////////////////////////////////
//
// PLL / interrupt-affected variables
//
/////////////////////////////////////

// all variables written to by the ISRs need the "volatile" keyword

// variables written to by ISRs
volatile unsigned int DSYNC_ISR_timestamp = 0;
volatile unsigned int PTACH_ISR_timestamp = 0;

// variables to tell code in the main loop to react to various interrupt events
volatile bool DSYNC_event_occurred = false; // use 'volatile' when variable is affected by ISR; https://www.arduino.cc/en/Reference/Volatile
volatile bool PTACH_event_occurred = false;
volatile bool VSYNC_event_occurred = false;

// our current (Rev. 1 Aug 2016) hardware needs a software divide-by-two
volatile bool DSYNC_divide_by_two_flag = true;
int           DSYNC_divider_inversion_count = 0; // this counter is used to see how long the phase loiters around +/-180 deg phase
unsigned char DSYNC_divider_inversion_event_count = 0; // this counter is simply used to show how often the division inversion occurs
bool          DSYNC_divider_inversion_occurred = false;

// the variables that make safe (atomic) copies of the ISR values, with the previous sample to take into account timer overflow
unsigned int DSYNC_ISR_timestamp_safe = 0; // safe copy of variables written to by ISRs
unsigned int PTACH_ISR_timestamp_safe = 0;
unsigned int DSYNC_ISR_timestamp_safe_last = 0;
unsigned int PTACH_ISR_timestamp_safe_last = 0;

// values to use in control algorithms
// define PHASE as PTACH - DSYNC because DSYNC is the reference signal with which we want to lock the motor
// positive phase means PTACH is occurring a little after DSYNC
// negative phase means PTACH is occurring a little before DSYNC
unsigned int DSYNC_period; // same as DSYNC_ISR_timestamp_safe, but adjusted for timer overflow
//unsigned int DSYNC_period_last; // delta of DSYNC not needed
unsigned int PTACH_period;
unsigned int PTACH_period_last;
         int PHASE_value = 0; // phase can be negative
         int PHASE_value_last = 0;
         int PHASE_delta = 0;
         int PHASE_delta_last = 0;
         int PHASE_acceleration = 0;
unsigned int PHASE_acceleration_zero_count = 0;

bool DSYNC_event_secondary = false;
//bool PTACH_event_secondary = false;
//bool PHASE_event_secondary = false;
bool         PHASE_updated = false; // indicates if PHASE has been updated in this main loop cycle



// filtering

unsigned char DSYNC_period_filter_factor = 5;
unsigned int  DSYNC_period_filtered = 0; // DSYNC_period filtered
unsigned long DSYNC_period_filtered_long_lshift = 0;
unsigned int  DSYNC_period_filtered_last = 0x1A57;
         int  DSYNC_period_filtered_half = 0;
         int  DSYNC_period_filtered_quarter = 0;
         int  DSYNC_period_filtered_eighth = 0;

unsigned char PTACH_period_filter_factor = 2;
unsigned int  PTACH_period_filtered = 0; // PTACH_period filtered
unsigned long PTACH_period_filtered_long_lshift = 0;
unsigned int  PTACH_period_filtered_last = 0;

bool          filter_PHASE = true;
unsigned char PHASE_value_filter_factor = 2;
  signed int  PHASE_value_filtered = 0; // PHASE_value filtered
  signed int  PHASE_value_filtered_last = 0;
  signed int /*long*/ PHASE_value_filtered_long_lshift = 0;