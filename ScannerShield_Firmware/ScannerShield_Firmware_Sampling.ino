/////////////////////////////////////
//
// VALUE PRINTING / DATA SAMPLING
//
/////////////////////////////////////

// this uses Serial.unsafe_direct_write_array(...), which is a modification of the Arduino Serial hardware library
// this is to minimize the number of clock cycles it takes to load data into the hardware serial transmit array
// modified by Andrew Henrie 29 July 2017
//   directory: C:\Users\[user]\AppData\Local\Arduino15\packages\arduino\hardware\avr\1.6.19\cores\arduino\
//   files modified:
//     HardwareSerial.h
//     HardwareSerial.cpp


// there are three types of memory on the Arduino Mega 2560:
//   Flash Memory 256 KB  non-volatile  (program space) is where the Arduino sketch is stored, of which 8 KB used by bootloader
//   SRAM           8 KB      volatile  (static random access memory) is where the sketch creates and manipulates variables when it runs
//   EEPROM         4 KB  non-volatile  memory space that programmers can use to store long-term information
// so the biggest (Flash) allows for 256-8 = 248kB
// the variables DSYNC_period, PHASE_value, PTACH_period are two bytes long and are updated at a max of 1.7kHz
// this gives us 248kB /( 3 * 2B * 1.7kHz ) = 24.89 seconds of record time
// the approximate size of this program is 
// flash is divided into 64k sections
// arrays probably can't process those sections boundaries
// arrays are indexed by signed 16-bit integers (max 32,768 elements)
// Erich Nygaard has experienc with reading from flash
// we can't write to flash unless we use a modified bootloader
// 
// what about Serial?
// the minimum baud rate we need is 3 * 16b * 1.7kHz = 81,600 bits/second
// that's if we're not using a terminal; not all ASCII characters are printable (some are delete/backspace)

// demarkers would be S (DSYNC), P (PHASE/PTACH)
//   PHASE and PTACH are updated on PTACH interrupt (INT5)
// baud 115,200 is standard
// baud 250,000 works, which is 104% more than 122,400 baud rate
//
// if we did hexadecimal, then SddddPpppptttt = 14 characters * 8b/B * 1.7kHz = 190,400 bits/second
// baud 250,000 is 31% more than 190,400 baud rate
//
// if we do sXXXXpXXXXtXXXX then we need 3*5 characters (bytes) * 8b/B * 1.7kHz = 204,000 bits/second baud rate
// if we do sXXXXpXXXXtXXXXfXXXX then we need 4*5 bytes * 8bits/byte * 1.7kHz = 272,000 bits/second baud rate



char print_str[] = "xXXXXYYYYZ"; // characters are placeholders



char getHexNybble( unsigned char n )
{
  if( n < 10 )
    return n + '0';
  if( n < 16 )
    return n - 10 + 'A';
  return '#';
}



char* printDecConstWidth( unsigned int n, bool is_signed )
{
  return printDecConstWidth( n, is_signed, print_str );
}



char* printDecConstWidth( unsigned int n, bool is_signed, char* char_buffer )
{
  // maximum 6 chars: _65535 or -32768
  
  if( is_signed && ( (signed int) n ) < 0 )
    n = -n;
  else
    is_signed = false;
  
  
  char order = 4;
  unsigned int ten_pow_order[] = { 1, 10, 100, 1000, 10000 };
  bool print_zeros = false;
  
  //     1 =                1
  //    10 =             1010
  //   100 =         01100100
  //  1000 =     001111101000
  // 10000 = 0010011100010000
  
  char_buffer[ 0 ] = ' ';
  char_buffer[ order + 2 ] = '\0';
  
  while( order >= 0 )
  {
    unsigned int digit = 0;
    unsigned int tpo = ten_pow_order[ order ];
    
    while( n >= tpo )
    {
      n -= tpo;
      digit++;
    }
    
    if( digit > 0 || print_zeros || order == 0 )
    {
      if( is_signed && !print_zeros )
        char_buffer[ 5 - order - 1 ] = '-';
      
      char_buffer[ 5 - order ] = digit + '0';
      print_zeros = true;
    }
    else
    {
      char_buffer[ 5 - order ] = ' ';
    }
    
    order--;
  }
  
  return char_buffer;
}



char* printCounterData( char t, unsigned int a )
{
  print_str[0] = t;
  print_str[1] = getHexNybble( 0x0f & ( a >> 12 ) );
  print_str[2] = getHexNybble( 0x0f & ( a >>  8 ) );
  print_str[3] = getHexNybble( 0x0f & ( a >>  4 ) );
  print_str[4] = getHexNybble( 0x0f &   a         );
  print_str[5] = '\0';

  return print_str;
}



char* printCounterData( char t, unsigned int a, unsigned int b )
{
  print_str[0] = t;
  print_str[1] = getHexNybble( 0x0f & ( a >> 12 ) );
  print_str[2] = getHexNybble( 0x0f & ( a >>  8 ) );
  print_str[3] = getHexNybble( 0x0f & ( a >>  4 ) );
  print_str[4] = getHexNybble( 0x0f &   a         );
  print_str[5] = getHexNybble( 0x0f & ( b >> 12 ) );
  print_str[6] = getHexNybble( 0x0f & ( b >>  8 ) );
  print_str[7] = getHexNybble( 0x0f & ( b >>  4 ) );
  print_str[8] = getHexNybble( 0x0f &   b         );
  // there's already a \0 at print_str[9] from the string initialization

  return print_str;
}



void send_telemetry()
{
  
  /*
  typedef struct
  {
    void* ptr;
    bool is_16b;
    bool is_signed;
    char c;
    char ccc[3];
    bool enabled;
    int prevval;
  } PrintVar;

  bool print_enabled = false; // enables/disables all printing
  unsigned char print_var_i = 0; // print variable index
  bool print_mode_human = true; // print mode
  bool print_mode_human_init = false;
  unsigned int print_cycles_budget = 0; // estimated number of clock cycles left
  */
  
  /*
  0         1         2         3         4         5         6         7         
  01234567890123456789012345678901234567890123456789012345678901234567890123456789
  FF -12345 P -12345 I -12345 D -12345 I -12345 T -12345
            1                 2        3
            get DSYNC/PTACH periods close enough so phase rate can be read
                              zero the phase rate
                                       align the phase

  DS      DSF     PT      PTF     FF      PeP     PeI     PhD     PhI     CMD
  -54321  -54321  -54321  -54321  -54321  -54321  -54321  -54321  -54321  -54321
    
   1  loop_total_cycles
   2  loop_computation_cycles
   3  motor_value
   4  DSYNC_period
   5  DSYNC_period_filtered
   6  PTACH_period
   7  PTACH_period_filtered
   8  PHASE_value
   9  PHASE_value_filtered
  10  PHASE_delta
  11  kp_period_term
  12  ki_period_term
  13  kd_phase_term
  14  ki_phase_term
  */
  
  if( print_mode_human )
  {
    // human printing mode
    
    if( print_mode_human_init )
    {
      print_mode_human_init = false;
    
      // start a new line
      Serial.unsafe_direct_write_array( "\r\n", 2 );
      
      // print variable labels
      n_print_vars_enabled = 0;
      for( print_var_i = 0; print_var_i < N_PRINT_VARS; print_var_i++ )
      {
        if( print_vars[ print_var_i ].enabled )
        {
          // print label (and tab over)
          char temp_c_str[ 4 ];
          temp_c_str[ 0 ] = print_vars[ print_var_i ].ccc[ 0 ];
          temp_c_str[ 1 ] = print_vars[ print_var_i ].ccc[ 1 ];
          temp_c_str[ 2 ] = print_vars[ print_var_i ].ccc[ 2 ];
          
          // tab over
          temp_c_str[ 3 ] = '\t';
          Serial.unsafe_direct_write_array( temp_c_str, 4 );
          
          n_print_vars_enabled++;
        }
      }
      
      Serial.unsafe_direct_write_array( "\r\n", 2 );
      
      print_var_i = 0;
    }
    
    // print variable numbers
    // check each variable once if we have time
    
    if( print_characters_budget >= PRINT_CHARS_MIN_THRESH )
    {      
      print_var_i_start_loop = print_var_i;
      
      do
      {
        if( print_vars[ print_var_i ].enabled )
        {
          char temp_c_str[8];// = "#+65535 \t";
        
          int temp_int = *( ( int* ) print_vars[ print_var_i ].ptr );
          
          // check to see if the variable has been updated
          if( print_vars[ print_var_i ].prevval != temp_int )
          {
            print_vars[ print_var_i ].prevval = temp_int;
            
            if( !print_vars[ print_var_i ].is_16b )
              temp_int = *( ( char* ) print_vars[ print_var_i ].ptr ); // cast as signed so it sign-extends into 2nd int byte
            
            temp_c_str[0] = '#';
            printDecConstWidth( ( unsigned int ) temp_int, print_vars[ print_var_i ].is_signed, temp_c_str + 1 );
            temp_c_str[7] = ' ';
            
            // print value
            print_characters_budget -= Serial.unsafe_direct_write_array( temp_c_str, 8 );
          }
          else
          {
            temp_c_str[0] = ' ';
            temp_c_str[1] = '\t';
            print_characters_budget -= Serial.unsafe_direct_write_array( temp_c_str, 2 );
          }
        }
      
        print_var_i++;
        if( print_var_i >= N_PRINT_VARS )
        {
          print_var_i = 0;
          print_characters_budget -= Serial.unsafe_direct_write_array( "\r\r", 2 );
        }
      }
      while( print_var_i_start_loop != print_var_i && print_characters_budget >= PRINT_CHARS_MIN_THRESH );
    }
  }
  else
  { // if( print_mode_human ) else
  
    // fast printing mode
    
    
    // as time allows,
    if( print_characters_budget >= PRINT_CHARS_MIN_THRESH )
    {
      // always check DSYNC_period[_filtered] first to see if it's been updated
      // this is so we con construct a stable timeline (always adding the value of DSYNC to a running "wall clock")
      
      // this method of time keeping might not always be used, so implement a method that doesn't reqire modifying the normal code
      int temp_int = *( ( int* ) print_vars[ PRINT_index_DSYNC_period ].ptr );
      
      if( print_vars[ PRINT_index_DSYNC_period ].prevval != temp_int )
      {
        noInterrupts();
        unsigned int temp_clock_start = TCNT3;
        interrupts();
        
        // "consume" the update
        print_vars[ PRINT_index_DSYNC_period ].prevval = temp_int;
        // DSYNC_period will never be printed out in the normal code by consuming the update here
        
        // print the value and subtract from our DSYNC period serial transmit budget
        char temp_c = print_vars[ PRINT_index_DSYNC_period ].c;
        print_characters_budget -= Serial.unsafe_direct_write_array( printCounterData( temp_c, ( unsigned int ) temp_int ), 5 );
        
        noInterrupts();
        unsigned int temp_clock_stop = TCNT3;
        interrupts();
        loop_print_cycles_accumulator += temp_clock_stop - temp_clock_start;
      }
    }
    
    
    if( print_characters_budget >= PRINT_CHARS_MIN_THRESH )
    {
      // go through each variable, looping back to the current variable
      print_var_i_start_loop = print_var_i;
      
      do
      {
        // if printing is enabled of this variable,
        if( print_vars[ print_var_i ].enabled )
        {          
          int temp_int = *( ( int* ) print_vars[ print_var_i ].ptr );
          
          // and it has been updated,
          if( print_vars[ print_var_i ].prevval != temp_int )
          {
            noInterrupts();
            unsigned int temp_clock_start = TCNT3;
            interrupts();
            
            // "consume" the update
            print_vars[ print_var_i ].prevval = temp_int;
            
            if( !print_vars[ print_var_i ].is_16b )
              temp_int = *( ( char* ) print_vars[ print_var_i ].ptr ); // cast as signed so it sign-extends into 2nd int byte
            
            // print the value and subtract from our DSYNC period serial transmit budget
            char temp_c = print_vars[ print_var_i ].c;
            print_characters_budget -= Serial.unsafe_direct_write_array( printCounterData( temp_c, ( unsigned int ) temp_int ), 5 );
            
            noInterrupts();
            unsigned int temp_clock_stop = TCNT3;
            interrupts();
            loop_print_cycles_accumulator += temp_clock_stop - temp_clock_start;
          }
        }
      
        // go onto the next variable
        print_var_i++;
        if( print_var_i >= N_PRINT_VARS )
          print_var_i = 0;
        
        // repeat until we have gone through each variable once or until we've run out of time
      }
      while( print_var_i_start_loop != print_var_i && print_characters_budget >= PRINT_CHARS_MIN_THRESH );
    }
  } // end else if( print_mode_human )
}