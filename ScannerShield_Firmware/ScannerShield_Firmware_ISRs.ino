/////////////////////////////////////
//
// Interrupt Service Routines
//
/////////////////////////////////////

// the vector name macros of MEGA 2560 are in:
// C:\Users\[user]\AppData\Local\Arduino15\packages\arduino\tools\avr-gcc\4.9.2-atmel3.5.4-arduino2\avr\include\avr\iomxx0_1.h

// BEGIN ICU DSYNC/PTACH

// Pin 48 / PL1 - DSYNC - ICP5
// Pin 49 / PL0 - PTACH - ICP4; old SPI DAC CS

// When an interrupt occurs, the Global Interrupt Enable I-bit is cleared and
// all interrupts are disabled. The user software can write logic one to the
// I-bit to enable nested interrupts. All enabled interrupts can then interrupt
// the current interrupt routine. The I-bit is automatically set when a Return
// from Interrupt instruction – RETI – is executed.
// ATmega2560 datasheet, Sec. 7.8, pg. 17

// enable global interrupts
//interrupts();
//__enable_interrupt();
//SREG |= 0x80; 

// under "normal" (broken-but-working) PLL locking conditions,
//   PTACH triggers at half the rate of DSYNC
//   PTACH and DSYNC are passed through divide-by-2 D-type flip-flops before being passed to the PLL chip

// with the Rev. 1 Aug 2016 Scanner Shield, DSYNC interrupt needs to be divided by two
// DSYNC is sensed from the output of the HW divider and not from the divide-by-2 flip-flop
// if we sensed it after the divide-by-two, we could trigger off of a change and be fine, but 
// we have to simulate that divide-by-two in software

// does this method takes a few more clock cycles?
// DSYNC_divide_by_two_flag = !DSYNC_divide_by_two_flag;

// note that we don't have to worry about the same interrupt being called multiple times within itself
// because PTACH and DSYNC occur at pseudo-regular intervals at low (<1.7kHz) frequency


// with Input Capture Units, DSYNC now (Fri 27 Oct 2017) measures:
//   10202  6%
//   10203 94% over 7266 samples
  
  
// DSYNC ISR
ISR( TIMER5_CAPT_vect )
{
  // noInterrupts(); - not needed; see Sec. 7.8, pg. 17 of datasheet
  DSYNC_ISR_timestamp = ICR5;
  // interrupts(); - not needed; see Sec. 7.8, pg. 17 of datasheet
  
  if( DSYNC_divide_by_two_flag )
  {
    DSYNC_event_occurred = true;
    // the main loop will clear this flag upon detecting it 

    DSYNC_divide_by_two_flag = false;
  }
  else
  {
    DSYNC_divide_by_two_flag = true;
  }
} // ISR( TIMER5_CAPT_vect )



// PTACH ISR
ISR( TIMER4_CAPT_vect )
{
  // noInterrupts(); - not needed; see Sec. 7.8, pg. 17 of datasheet
  PTACH_ISR_timestamp = ICR4;
  // interrupts(); - not needed; see Sec. 7.8, pg. 17 of datasheet
  
  // indicate to main loop that this event has occurred
  // so that it can do non-time-critical work with the values
  PTACH_event_occurred = true;
  // the main loop will clear this flag upon detecting it

} // ISR( TIMER4_CAPT_vect )



// VSYNC ISR
ISR( INT2_vect )
{
  // VSYNC isn't as critical to get accurate as DSYNC or PTACH; let those
  // interrupts have priority if they occur in here.
  // Also, VSYNC_event_occurred is a boolean, which occupies a single byte;
  // interrupts can't occur within a byte write (they can in a multi-byte write).

  // enable global interrupts
  interrupts();
  //__enable_interrupt();
  //SREG |= 0x80;
  // see Sec. 7.8, pg. 17 of datasheet

  VSYNC_event_occurred = true;
} // ISR(INT2_vect) (VSYNC ISR)