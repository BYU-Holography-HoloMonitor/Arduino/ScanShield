/////////////////////////////////////
//
// SETUP VARIABLES
//
/////////////////////////////////////

// Arduino MEGA 2560 shield pinout: https://lynx2015.files.wordpress.com/2015/08/arduino-mega-pinout-diagram.png
// digital aliases of analog pins: http://forum.arduino.cc/index.php?topic=13779.msg102414#msg102414

// display parameters
// this value will need to be adjusted when the number of HoloLines changes
// GALVO_DELTA = 4095 / N_HOLOLINES, ie, 78 = int( 4095 / 52 )
#define GALVO_DELTA 78

// pins labelled with Arduino MEGA 2560 shield pin; ATMEGA 2560 chip pin is listed as "uC", with relevant special function listed as "func"

// interrupts
#define PIN_DSYNC 48 // digital input; uC PL1; func ICP5; (for Rev. 26 Sep 2017 Scanner Shield), PIN_DSYNC was on 2 (for Rev. 1 Aug 2016 Scanner Shield)
#define PIN_PTACH 49 // digital input; uC PL0; func ICP4; (for Rev. 26 Sep 2017 Scanner Shield), PIN_PTACH was on 3 (for Rev. 1 Aug 2016 Scanner Shield)
#define PIN_HSYNC 18 // digital input; uC PD3; func INT3
#define PIN_VSYNC 19 // digital input; uC PD2; func INT2

// DVI breakout
#define PIN_HOT_PLUG 22 // digital output; uC PA0
boolean PIN_HOT_PLUG_state = HIGH; // enable GPU DVI output on startup/connection
#define PIN_MIXAMP0 24 // digital input or output; uC PA2
#define PIN_MIXAMP1 25 // digital input or output; uC PA3
#define PIN_MIXAMP2 23 // digital input or output; uC PA1

// PWM & Polygon Control
#define PIN_ESC_PWM     4  // digital output; uC PG5; PWM
#define PIN_POLY_ENABLE 45 // digital output; uC PL4; PWM
boolean PIN_POLY_ENABLE_state = 1; // 0 on, 1 off (low-assertion)
#define PIN_POLY_LOCKED 46 // digital input; uC PL3

// DAC
#define PIN_DAC_SPI_CS 47 // digital output; uC PL2; moved from 49 (Scanner Shield board Rev. 1 Aug 2016) for Input Capture Units (Scanner Shield board Rev. 26 Sep 2017) // out
#define DAC_CH_GALVO 0
#define DAC_CH_POLY  1
int galvo_value = 0;    // keep galvo_value and motor_value as signed ints and not unsigned ints
int motor_value = 3183; // this is to allow for signed comparisons to still work (ie, motor_value--; if( motor_value < 0 ) motor_value = 0; )
                        // if these were made unsigned, the above comparison would never work, as motor_value would go from 1 to 0 to 65,535, all of which are positive
                        // 3183 is a good starting point for manual polygon control
int motor_value_prev = motor_value; // update the motor value on changes

// Auxiliary Circuits for Rev. 26 Sep 2017 board
#define PIN_BUZZER      63  // digital output;  uC PK1; aka analog pin A9
#define PIN_TEMP_SENSOR  5  // analog input A5; uC PF5; aka digital pin 59
int temp_sensor_val;
#define PIN_LED_ACT_SPI 69  // digital output;  uC PK7; aka analog pin A15; GREEN  LED is near DAC
boolean PIN_LED_ACT_SPI_state = 0;
#define PIN_LED_ACT_I2C 14  // digital output;  uC PJ1;                     WHITE  LED is between PLL potentiometers and DVI Breakout port
boolean PIN_LED_ACT_I2C_state = 0;
#define PIN_LED_HSYNC   13  // digital output;  uC PB7; PWM;                ORANGE LED is near AMPL galvo potentiometer
boolean PIN_LED_HSYNC_state = 0;
#define PIN_LED_VSYNC   12  // digital output;  uC PB6; PWM;                BLUE   LED is near galvo output port
boolean PIN_LED_VSYNC_state = 0;