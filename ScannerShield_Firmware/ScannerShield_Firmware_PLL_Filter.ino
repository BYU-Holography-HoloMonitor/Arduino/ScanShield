void DSYNC_update_actions()
{
  // actions:
  // - start counting clock cycles used for computations (measurements, filtering, control, etc...)
  // - save new DSYNC timestamp
  // - calculate DSYNC period
  // - calculated filtered DSYNC period and derivative values
  // - update printout budget and computational load values

  
  // Concerning hardware improvements, this main loop ISR response code
  // should be hardware-version agnostic; accepting counter values as correct.
  // In a future (beyond Fri 28 Jul 2017) board version, the technique for
  // measuring the timestamp of DSYNC and PTACH events will produce more accurate
  // results. Specifically, timer timestamp values for the DSYNC and PTACH events,
  // for when they occur in the same clock cycle, should be equal by using Input
  // Capture Units in a later (> Rev. 1 Aug 2016) board version. This main loop
  // ISR response code here should be written for the ideal (perfectly-measured)
  // case so that the minimal amount of code needs to be changed per board revision.
  // However, dead-zone code may be used here to round small errors to zero to
  // to prevent controller integrator terms from continuously changing when locked.
  //  - Andrew Henrie, Fri 28 Jul 2017
  
  
  
  // get a safe copy (double-buffered) of the counter value
  
  DSYNC_ISR_timestamp_safe_last = DSYNC_ISR_timestamp_safe;
  
  noInterrupts();
  // warning: DSYNC_ISR_timestamp should only be read when DSYNC_event_occurred has been recently set
  // DSYNC_ISR_timestamp may be set when DSYNC_even_occurred has not been set
  // see the implementation of the DSYNC ISR
  DSYNC_ISR_timestamp_safe = DSYNC_ISR_timestamp;
  interrupts();

  
  
  // get new DSYNC_period
  // this operation is unaffected by counter overflow
  // DSYNC_period_last = DSYNC_period; // delta of DSYNC not needed
  
  DSYNC_period = DSYNC_ISR_timestamp_safe - DSYNC_ISR_timestamp_safe_last;
  print_vars[ PRINT_index_DSYNC_period ].prevval = DSYNC_period + 1; // always print DSYNC_period for timestamps
  
  
  
  // update DSYNC_period_filtered using running average
  // a bit-shift of 5 produces an alpha of 0.03125
  // temporarily promote variables to long to preserve fidelity while averaging
  // keep a running copy of the long left-shifted value to preserve fidelity

  DSYNC_period_filtered_long_lshift += (int) DSYNC_period - (int) DSYNC_period_filtered;
  //DSYNC_period_filtered_long_lshift -= (unsigned long) DSYNC_period_filtered;
  DSYNC_period_filtered = DSYNC_period_filtered_long_lshift >> DSYNC_period_filter_factor;
  
  // we use fractional values of DSYNC_period_filtered often, so just compute
  // this once so we don't have to recompute it multiple times
  if( DSYNC_period_filtered != DSYNC_period_filtered_last )
  {
    DSYNC_period_filtered_last = DSYNC_period_filtered;
    DSYNC_period_filtered_half    = (int) ( DSYNC_period_filtered >> 1 );
    DSYNC_period_filtered_quarter = (int) ( DSYNC_period_filtered >> 2 );
    DSYNC_period_filtered_eighth  = (int) ( DSYNC_period_filtered >> 3 );
  }
  
  

  // reset the budget counter of how many characters we can print this DSYNC period
  
  print_characters_budget = PRINT_CHARS_BUDGET;
  
  // reset computation clock cycle counter
  
  loop_computation_cycles = loop_computation_cycles_accumulator;
  loop_computation_cycles_accumulator = 0;
  
  loop_print_cycles = loop_print_cycles_accumulator;
  loop_print_cycles_accumulator = 0;
  
  loop_total_cycles = loop_computation_cycles + loop_print_cycles;
  
  //// record the number of clock cycles available from last DSYNC period
  
  //DSYNC_period_cycles_surplus   = DSYNC_period_cycles_remaining;
  //DSYNC_period_cycles_remaining = DSYNC_period_filtered;
  

  
  // if you want to know what your "NOW" clock cycle budget is, calculate ( TCNT3 - DSYNC_ISR_timestamp_safe )
}





void PTACH_update_actions()
{
  // actions:
  // - save new PTACH timestamp
  // - jog the galvo
  // - calculate PTACH period
  // - calculate filtered PTACH period
  // - calculate new phase
  // - trigger phase update actions
  
  
  
  // get a safe copy (double-buffered) of counter values
    
  PTACH_ISR_timestamp_safe_last = PTACH_ISR_timestamp_safe;
  
  noInterrupts();
  PTACH_ISR_timestamp_safe = PTACH_ISR_timestamp;
  interrupts();
  
  
  
  // jog the galvo position a step
  // this should be tied to PTACH and not DSYNC because PTACH comes from mirror position; DSYNC doesn't
  // (tie optically-scanning elements to optically-scanning elements)
  
  galvo_value += GALVO_DELTA;
  if( galvo_value > 4095 )
    galvo_value = 4095;
    
  dac_write( DAC_CH_GALVO, galvo_value );

  

  // get new PTACH_period
  // this operation is unaffected by counter overflow
  
  PTACH_period_last = PTACH_period;
  PTACH_period = PTACH_ISR_timestamp_safe - PTACH_ISR_timestamp_safe_last;
  
  
  
  // update PTACH_period_filtered using running average
  // a bit-shift of 3 produces an alpha of 0.125
  // temporarily promote variables to long to preserve fidelity while averaging
  // keep a running copy of the long left-shifted value to preserve fidelity
  
  PTACH_period_filtered_last = PTACH_period_filtered;
  
  PTACH_period_filtered_long_lshift += (int) PTACH_period - (int) PTACH_period_filtered;
  //PTACH_period_filtered_long_lshift -= (unsigned long) PTACH_period_filtered;
  PTACH_period_filtered = (unsigned int) ( PTACH_period_filtered_long_lshift >> PTACH_period_filter_factor );
  
  
  
  // get new PHASE_value
  
  // measuring the phase from the current PTACH timestamp to the last DSYNC timestamp is
  // desirable because:
  //  - it's simple
  //  - it can always be used to calculate the current phase
  //  - the last phase sample can always be used to calculate the rate of change of phase
  //  - it always works regardless of the rate of change of phase (positive or negative),
  //      including close to the non-aliasing limits ( -DSYNC/2, DSYNC/2 )
  //  - it seamlessly handles loop-around, for both phase and phase delta
  //  - two-part code implementation (DSYNC+PTACH) is not needed
  //  - zero phase can be equivalently measured from either the current or previous DSYNC
  
  PHASE_value_last = PHASE_value;
  
  PHASE_value = PTACH_ISR_timestamp_safe - DSYNC_ISR_timestamp_safe;
  
  // the above technique of measuring phase can sometimes lead to negative values
  // this is easily corrected by adding a complete cycle
  if( PHASE_value < 0 )
    PHASE_value += DSYNC_period_filtered;
  
  // shift the phase scale from "0 to 360" to "-180 to 180"
  // this is easier to work with in the control theory algorithm ("0 = no error" instead of "180 = no error")
  if( PHASE_value > DSYNC_period_filtered_half )
    PHASE_value -= DSYNC_period_filtered;
  
  PHASE_updated = true;
}





void PHASE_update_actions()
{
  // actions:
  // - calculate filtered phase
  // - calculate phase delta
  // - check for phase inversion
  //
  


/*long*/int DSYNC_period_filtered_long_lshift_for_PHASE_filter = DSYNC_period_filtered;
    DSYNC_period_filtered_long_lshift_for_PHASE_filter <<= PHASE_value_filter_factor;
    
    
    if( DSYNC_divider_inversion_occurred )
    {
      DSYNC_divider_inversion_occurred = false;
      
      // there's been a DSYNC divider inversion event, so fix the filtered phase to take that into account
      // make it look like the filtered phase was already correct
      
      /*
      if( PHASE_value_filtered > 0 )
      {
        PHASE_value_filtered             -=  DSYNC_period_filtered_half;
        PHASE_value_filtered_long_lshift -= (DSYNC_period_filtered_long_lshift_for_PHASE_filter >> 1);
      }
      else
      {
        PHASE_value_filtered             +=  DSYNC_period_filtered_half;
        PHASE_value_filtered_long_lshift += (DSYNC_period_filtered_long_lshift_for_PHASE_filter >> 1);
      }
      */
    }
    

    // update PHASE_value_filtered using a cyclic running average
    
    PHASE_value_filtered_last = PHASE_value_filtered;    
    
    
//    if( filter_PHASE )
//    {
      
      int PHASE_value_temp = PHASE_value;
      
      int PHASE_filter_normal_diff = PHASE_value_temp - PHASE_value_filtered;
      
  /*     if( PHASE_filter_report )
      {
        Serial.println();
      
        Serial.println( "Z: " + String( DSYNC_period_filtered_half ) );
        Serial.println( "Y: " + String( DSYNC_period_filtered ) );
        Serial.println( "X: " + String( PHASE_value_temp ) );
        Serial.println( "W: " + String( PHASE_value_filtered ) );
        Serial.println( "A: " + String( PHASE_filter_normal_diff ) );
        Serial.println( "B: " + String( DSYNC_period_filtered_long_lshift_for_PHASE_filter ) );
      } */
      
      while( PHASE_filter_normal_diff >  DSYNC_period_filtered_half )
      {
        //PHASE_value_temp -= DSYNC_period_filtered;
        PHASE_filter_normal_diff -= DSYNC_period_filtered;
        
  /*       if( PHASE_filter_report )
          Serial.println( "Ca" ); */
      }
      
      while( PHASE_filter_normal_diff < -DSYNC_period_filtered_half )
      {
        //PHASE_value_temp += DSYNC_period_filtered;
        PHASE_filter_normal_diff += DSYNC_period_filtered;
        
  /*       if( PHASE_filter_report )
          Serial.println( "Cb" ); */
      }
      
  /*     if( PHASE_filter_report )
      {
        Serial.println( "V: " + String( PHASE_value_temp ) );
        Serial.println( "D: " + String( PHASE_value_filtered_long_lshift ) );
      } */
        
      PHASE_value_filtered_long_lshift += PHASE_filter_normal_diff;
      //PHASE_value_filtered_long_lshift += ( PHASE_value_temp - PHASE_value_filtered );
      //PHASE_value_filtered_long_lshift -= PHASE_value_filtered;
      PHASE_value_filtered = PHASE_value_filtered_long_lshift >> PHASE_value_filter_factor;
      
      
  /*     if( PHASE_filter_report )
      {
        Serial.println( "E: " + String( PHASE_value_filtered_long_lshift ) );
        Serial.println( "F: " + String( PHASE_value_filtered ) );
        Serial.println( "G: " + String( DSYNC_period_filtered_long_lshift_for_PHASE_filter ) );
      } */
      
      while( PHASE_value_filtered >  DSYNC_period_filtered_half )
      {
        PHASE_value_filtered             -= DSYNC_period_filtered;
        PHASE_value_filtered_long_lshift -= DSYNC_period_filtered_long_lshift_for_PHASE_filter;
        
  /*       if( PHASE_filter_report )
          Serial.println( "Ha" ); */
      }
      
      while( PHASE_value_filtered < -DSYNC_period_filtered_half )
      {
        PHASE_value_filtered             += DSYNC_period_filtered;
        PHASE_value_filtered_long_lshift += DSYNC_period_filtered_long_lshift_for_PHASE_filter;
        
  /*       if( PHASE_filter_report )
          Serial.println( "Hb" ); */
      }
      
      
//    }
//    else
//    {    
//      PHASE_value_filtered = PHASE_value;
//    } // end if( filter_PHASE )
    
    
    
    // get new PHASE_delta
    
    // note that PHASE_delta (new phase - old phase) = PTACH_period - DSYNC_period
    // but PHASE_delta *aliases* if:
    //   PTACH_period - DSYNC_period < -DSYNC_period / 4   ||   DSYNC_period / 4 < PTACH_period - DSYNC_period
    
    // base the delta off of the filter to piggy back filtering
    
    PHASE_delta_last = PHASE_delta;
    
    PHASE_delta = PHASE_value_filtered - PHASE_value_filtered_last;
    
    while( PHASE_delta >  DSYNC_period_filtered_half )
      PHASE_delta -= DSYNC_period_filtered;
    
    while( PHASE_delta < -DSYNC_period_filtered_half )
      PHASE_delta += DSYNC_period_filtered;
    
    PHASE_acceleration = PHASE_delta - PHASE_delta_last;
    
    
    
    // begin DSYNC software divider phase check
    {
      
      // check to see if our software DSYNC ISR divider is in the wrong phase
      // flip it if it is
      
      // do this by counting the amount of time it spends above a positive phase margin or below a negative phase margin
      // if we spend too much time close to +/- 180 degrees, invert the divider phase
      
      int DSYNC_period_filtered_margin = DSYNC_period_filtered_half - ( DSYNC_period_filtered_half >> 4 );
      if( PHASE_value < -DSYNC_period_filtered_margin || DSYNC_period_filtered_margin < PHASE_value )
      {
        // this could indicate that our software divide-by-two for the DSYNC is out of phase
        
        DSYNC_divider_inversion_count++;
        
        if( DSYNC_divider_inversion_count >= 200 )
        {
          DSYNC_divider_inversion_count = 0;
          
          noInterrupts();
          DSYNC_divide_by_two_flag = !DSYNC_divide_by_two_flag;
          interrupts();
          
          Serial.println( "\ndiv inv" );
          DSYNC_divider_inversion_event_count++; // we don't care about overflow; we're just monitoring changes
          
          // if a counter inversion occurs, the phase and derivative values (phase filtered, phase delta) will need to be adjusted / not updated
          DSYNC_divider_inversion_occurred = true;
        }
      }
      else
      {
        if( DSYNC_divider_inversion_count > 0 )
          DSYNC_divider_inversion_count--;
      }
    }
    // end DSYNC software divider phase check
}